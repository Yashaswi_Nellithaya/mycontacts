package com.yashaswi.mycontacts.ui.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.auth0.android.Auth0
import com.auth0.android.Auth0Exception
import com.auth0.android.provider.VoidCallback
import com.auth0.android.provider.WebAuthProvider
import com.yashaswi.mycontacts.MyContactsApplication
import com.yashaswi.mycontacts.R
import com.yashaswi.mycontacts.dtos.ContactsDTO
import com.yashaswi.mycontacts.ui.adapters.ContactsAdapter
import com.yashaswi.mycontacts.ui.listeners.OnItemClickListener
import com.yashaswi.mycontacts.utils.AppConstants
import com.yashaswi.mycontacts.utils.PreferenceUtils
import com.yashaswi.mycontacts.viewmodels.ContactsViewModel
import kotlinx.android.synthetic.main.activity_main.*

class HomeListingActivity : AppCompatActivity() {

    private lateinit var contactsAdapter: ContactsAdapter
    private lateinit var contactViewModel: ContactsViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initView()

    }

    private fun initView() {
        assignViewModels()
        subscribeViewModels()
        contactsRV.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        contactsAdapter = ContactsAdapter(this, onContactClickListener)
        contactsRV.adapter = contactsAdapter
    }

    private fun subscribeViewModels() {
        contactViewModel.getContacts().observe(this, Observer { contacts ->
            if (contacts.size > 0)
                contactsAdapter.setDataAndRefresh(contacts)
        })
    }

    private fun assignViewModels() {
        contactViewModel = ViewModelProvider(this).get(ContactsViewModel::class.java)
    }

    private val onContactClickListener: OnItemClickListener<ContactsDTO> =
        object : OnItemClickListener<ContactsDTO> {
            override fun onItemClick(item: ContactsDTO) {
                //TODO if click needed
            }
        }
}

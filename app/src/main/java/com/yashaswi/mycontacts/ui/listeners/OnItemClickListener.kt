package com.yashaswi.mycontacts.ui.listeners

/**
 * Created by Yashaswi N P on 30/5/20.
 */
interface OnItemClickListener<T> {
    /**
     * Invokes once the item is clicked
     */
    fun onItemClick(item: T)
}
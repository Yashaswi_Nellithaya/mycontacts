package com.yashaswi.mycontacts.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.yashaswi.mycontacts.R
import com.yashaswi.mycontacts.dtos.ContactsDTO
import com.yashaswi.mycontacts.ui.listeners.OnItemClickListener
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.layout_contacts_item.view.*

/**
 * Created by Yashaswi N P on 30/5/20.
 */

class ContactsAdapter(context: Context, clickListener: OnItemClickListener<ContactsDTO>) :
    RecyclerView.Adapter<ContactsAdapter.ContactsViewHolder>() {

    private val mContext: Context = context
    private var items = ArrayList<ContactsDTO>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactsViewHolder {
        val view =
            LayoutInflater.from(mContext).inflate(R.layout.layout_contacts_item, parent, false)
        return ContactsViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ContactsViewHolder, position: Int) {
        if (items.isNotEmpty()) {
            val contact = items[position]
            Glide.with(mContext)
                .load(contact.profileImage)
                .placeholder(R.drawable.placeholder)
                .into(holder.mContactImageIV)
            holder.mContactName.text = contact.firstName.plus(contact.lastName)
        }
    }

    /**
     * to get the data from activities / fragments
     */
    fun setDataAndRefresh(contactsList: ArrayList<ContactsDTO>) {
        this.items = contactsList
        notifyDataSetChanged()
    }

    inner class ContactsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener {
        val mContactImageIV: CircleImageView = itemView.contactImageIV
        val mContactName: TextView = itemView.contactsNameTV

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
//            mOnClickListener.onItemClick(items[adapterPosition])
        }
    }
}
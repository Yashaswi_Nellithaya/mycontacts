package com.yashaswi.mycontacts.ui.activities

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.auth0.android.Auth0Exception
import com.auth0.android.authentication.AuthenticationException
import com.auth0.android.provider.AuthCallback
import com.auth0.android.provider.VoidCallback
import com.auth0.android.provider.WebAuthProvider
import com.auth0.android.result.Credentials
import com.yashaswi.mycontacts.MyContactsApplication
import com.yashaswi.mycontacts.R
import com.yashaswi.mycontacts.utils.AppConstants.EXTRA_CLEAR_CREDENTIALS
import com.yashaswi.mycontacts.utils.PreferenceUtils


class LoginActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_login)
//        initAuthO()
        initView()
    }

    private fun initView() {
        extractData()
        logIn()
    }

//
//    private fun initAuthO() {
//        mAuthO = Auth0(this)
//        mAuthO.isOIDCConformant = true
//    }

//
    private fun extractData() {
        if (intent.getBooleanExtra(EXTRA_CLEAR_CREDENTIALS, false)) {
            logoutUser()
        }
    }

    private fun logIn() {
        MyContactsApplication.mAuthOInstance?.let {
            WebAuthProvider.login(it)
                .withScheme("demo")
                .withAudience(
                    String.format(
                        "https://%s/userinfo",
                        getString(R.string.com_auth0_domain)
                    )
                )
                .start(this, object : AuthCallback {
                    override fun onFailure(dialog: Dialog) {
                        dialog.show()
                    }

                    override fun onFailure(exception: AuthenticationException?) {
                        // Show error to user
                        Log.e("Login Exception", exception?.message.toString())
                    }

                    override fun onSuccess(credentials: Credentials) {
                        PreferenceUtils.setAccessToken(credentials.accessToken.toString())
                        startActivity(Intent(this@LoginActivity, HomeListingActivity::class.java))
                    }
                })
        }
    }


    private fun logoutUser() {
        MyContactsApplication.mAuthOInstance?.let {
            WebAuthProvider.logout(it)
                .withScheme("demo")
                .start(this, object : VoidCallback {
                    override fun onSuccess(payload: Void) {
                        PreferenceUtils.clearAccessToken()
                    }

                    override fun onFailure(error: Auth0Exception) {
                        Log.e("logout Exception", error.message.toString())

                    }
                })
        }
    }

}

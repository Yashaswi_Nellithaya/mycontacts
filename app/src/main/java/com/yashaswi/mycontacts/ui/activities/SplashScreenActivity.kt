package com.yashaswi.mycontacts.ui.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import com.yashaswi.mycontacts.utils.PreferenceUtils

class SplashScreenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Handler().postDelayed({
            if (TextUtils.isEmpty(PreferenceUtils.getAccessToken()))
                startActivity(Intent(this, LoginActivity::class.java))
            else
                startActivity(Intent(this, HomeListingActivity::class.java))
        }, 2000)
    }

}
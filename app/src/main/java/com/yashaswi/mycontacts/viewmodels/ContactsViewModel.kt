package com.yashaswi.mycontacts.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.yashaswi.mycontacts.dtos.ContactsDTO
import com.yashaswi.mycontacts.respositories.ContactsRepository

/**
 * Created by Yashaswi N P on 30/5/20.
 */

class ContactsViewModel : ViewModel() {
    private val contactsRepository = ContactsRepository()

    fun getContacts(): LiveData<ArrayList<ContactsDTO>> {
        return contactsRepository.fetchContacts()
    }

}
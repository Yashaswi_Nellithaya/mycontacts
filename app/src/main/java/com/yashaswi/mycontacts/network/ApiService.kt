package com.yashaswi.mycontacts.network

import com.yashaswi.mycontacts.dtos.ContactsBaseDTO
import com.yashaswi.mycontacts.dtos.ContactsDTO
import com.yashaswi.mycontacts.utils.AppConstants
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET
import java.util.*

/**
 * Created by Yashaswi N P on 30/5/20.
 */
interface ApiService {


    @GET(AppConstants.GET_CONTACTS)
    fun getRemoteContacts(): Observable<Response<ContactsBaseDTO>>

}
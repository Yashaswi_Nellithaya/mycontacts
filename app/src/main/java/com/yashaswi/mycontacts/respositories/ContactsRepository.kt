package com.yashaswi.mycontacts.respositories

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.yashaswi.mycontacts.dtos.ContactsDTO
import com.yashaswi.mycontacts.network.ApiComponent
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * Created by Yashaswi N P on 30/5/20.
 */

class ContactsRepository {
    private var TAG = javaClass.simpleName
    private var apiComponent = ApiComponent
    lateinit var disposable: Disposable

    fun fetchContacts(): MutableLiveData<ArrayList<ContactsDTO>> {
        return fetchContactsFromApi()
    }

    private fun fetchContactsFromApi(): MutableLiveData<ArrayList<ContactsDTO>> {
        val contactResponse = MutableLiveData<ArrayList<ContactsDTO>>()
        disposable = apiComponent.getApiInterface().getRemoteContacts()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.isSuccessful) {
                    contactResponse.value = it.body()?.data
                }
            }, {
                disposable.dispose()
                contactResponse.value = null
                Log.e(TAG, it.message.toString())
            }, {
                disposable.dispose()
            })
        return contactResponse

    }
}
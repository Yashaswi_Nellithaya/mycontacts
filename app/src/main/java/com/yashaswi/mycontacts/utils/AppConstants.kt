package com.yashaswi.mycontacts.utils

/**
 * Created by Yashaswi N P on 29/5/20.
 */

object AppConstants {

    const val EXTRA_CLEAR_CREDENTIALS: String = "EXTRA_CLEAR_CREDENTIALS"

    //network contacts
    const val REQUEST_READ_TIME_OUT_IN_SECONDS: Long = 30
    const val REQUEST_WRITE_TIME_OUT_IN_MINUTES: Long = 1
    const val BASE_URL = "https://7yd7u01nw9.execute-api.ap-south-1.amazonaws.com/"
    const val GET_CONTACTS = "prod/contact-list"
}
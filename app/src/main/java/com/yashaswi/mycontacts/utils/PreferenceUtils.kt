package com.yashaswi.mycontacts.utils

import android.app.Activity
import android.content.SharedPreferences
import com.yashaswi.mycontacts.MyContactsApplication

/**
 * Created by Yashaswi N P on 29/5/20.
 */

object PreferenceUtils {

    private const val ACCESS_TOKEN = "ACCESS_TOKEN"

    private fun editPreference(editorMethod: (SharedPreferences.Editor) -> Unit) {
        val editor = MyContactsApplication.applicationInstance?.getSharedPreferences(
            MyContactsApplication.applicationInstance?.packageName,
            Activity.MODE_PRIVATE
        )?.edit()
        editor?.run {
            editorMethod(editor)
            editor.apply()
        }
    }

    private fun <T> getPreference(getMethod: (SharedPreferences) -> T, defaultValue: T): T {
        val sharedPreferences = MyContactsApplication.applicationInstance?.getSharedPreferences(
            MyContactsApplication.applicationInstance?.packageName,
            Activity.MODE_PRIVATE
        )
        return sharedPreferences?.run(getMethod) ?: defaultValue
    }

    private fun putString(key: String, value: String?) {
        editPreference { it.putString(key, value) }
    }

    private fun getString(key: String, defValue: String?): String? {
        return getPreference({ it.getString(key, defValue).toString() }, defValue)
    }


    /**
     * sets is the access token of the app
     */
    fun setAccessToken(token: String) {
        putString(ACCESS_TOKEN, token)
    }

    /**
     * gets the access token
     */
    fun getAccessToken(): String? {
        return getString(ACCESS_TOKEN, null)
    }

    /**
     * gets the access token
     */
    fun clearAccessToken() {
        return putString(ACCESS_TOKEN, null)
    }
}
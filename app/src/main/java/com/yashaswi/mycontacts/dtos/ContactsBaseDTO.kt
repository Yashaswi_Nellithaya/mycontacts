package com.yashaswi.mycontacts.dtos

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable

/**
 * Created by Yashaswi N P on 30/5/20.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
data class ContactsBaseDTO(
    @JsonProperty("success") var success: Int? = null,
    @JsonProperty("msg") var msg: String? = null,
    @JsonProperty("data") var data: ArrayList<ContactsDTO>? = null
) : Serializable

package com.yashaswi.mycontacts.dtos

import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable

/**
 * Created by Yashaswi N P on 30/5/20.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
data class ContactsDTO(
    @JsonProperty("first_name") var firstName: String? = null,
    @JsonProperty("last_name") var lastName: String? = null,
    @JsonProperty("mobile_number") var mobileNumber: Int? = null,
    @JsonProperty("email_id") var emailId: String? = null,
    @JsonProperty("profile_image") var profileImage: String? = null
) : Serializable
package com.yashaswi.mycontacts

import android.app.Application
import com.auth0.android.Auth0

/**
 * Created by Yashaswi N P on 30/5/20.
 */

class MyContactsApplication : Application() {


    override fun onCreate() {
        super.onCreate()
        initAuthO()
        applicationInstance = this
    }

    private fun initAuthO() {
        mAuthOInstance = Auth0(this)
        mAuthOInstance?.isOIDCConformant = true
    }


    companion object {
        var applicationInstance: MyContactsApplication? = null
        var mAuthOInstance: Auth0? = null
    }
}